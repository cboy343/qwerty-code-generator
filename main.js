$(document).ready(function() {
    $(document).keypress(function(e){
        /** Depending on which text field is focused, pressing enter will start the program */
        if (e.which == 13) {
            if ($('#how_many').is(':focus')) {
                $('#start').click();
            } else if ($('#get_card').is(':focus')) {
                $('#find_card').click();
            }
        }
    });

    $('#find_card').click(function() {
        /** Recreate a card that a user types in */
        const input = $('#get_card').val();
        if (input.length == 34 && isNaN(input)) {
            find_card(input);
        } else {
            $('#how_many, #get_card').val('');
            $('#get_card').focus();
        }
    });

    $('#start').click(function() {
        /** Click listener for generating new cards */
        const input = Number($('#how_many').val());
        if (1 < input && input < 100 && Number.isInteger(input)) {
            make_id(input);
        } else {
            $('#how_many, #get_card').val('');
            $('#how_many').focus();
        }
    });

    $('body').on('click', '.print_button', function(){
        /** Click listener for print buttons */
        let card_id = $(this).attr('data-card');
        let card = $('#'+card_id);
        let card_code = card.attr("data-card_details");
        let card_code_array = card_code.split(',');
        let recover_text = card_code_array.join('');
        let spacebar_code = card_code_array.pop();
        create_print_page(card_code_array, card, spacebar_code, recover_text);

    });

});

function make_id(how_many) {
    /** Generate n number of cards. */
    $('#start, #how_many, #get_card, #find_card').remove()
    for (card_number = 0; card_number < how_many; card_number++) {
        let card_replacements = [];
        while (card_replacements.length < 26) {
            let current_character = generate_random_character(); 
            if (card_replacements.indexOf(current_character) == -1) {
                card_replacements.push(current_character);
            }
        }
        card_replacements = add_letters_to_card(card_number, card_replacements);
        let spacebar_code = '';
        for (i = 0; i < 8; i++) {
            let character = generate_random_character();
            spacebar_code += character;
        }
        card_replacements.push(spacebar_code);
        $('#'+card_number).attr('data-card_details', card_replacements);
        $('#'+card_number).append('<p><span class="spacebar_code">'+spacebar_code+'</span><p>');
    }
}

function add_letters_to_card(card_number, card_replacements, is_find=false) {
    /** Put's letters onto a PNG on a card */
    let ordered_characters = [];
    if (is_find) {
        $('#the_print').append('<div id="for_printing_one" class="inline_block"><div id="'+card_number+'" class="a_card card_size"><img draggable="false" class="card_picture card_size" src="qwerty_card.svg"></img></div><br></div>');
        ordered_characters = card_replacements.slice();
        card_replacements = card_replacements.reverse();
    } else {
        $('#cards').append('<div class="inline_block"><div id="'+card_number+'" class="a_card card_size"><img draggable="false" class="card_picture card_size" src="qwerty_card.svg"></img></div><br><button data-card="'+card_number+'" class="print_button">Print Card</button></div>');
    }
    
    for (replaced_letter = 0; replaced_letter < 26; replaced_letter++) {
        if (replaced_letter < 10) {
            var x_position = 30 + 29 * replaced_letter;
            var y_position = '41px';
        } else if (10 <= replaced_letter && replaced_letter < 19) {
            let remove_extra = replaced_letter - 10;
            var x_position = 43 + 29 * remove_extra;
            var y_position = '89px';
        } else {
            let remove_extra = replaced_letter - 19;
            var x_position = 73 + 29 * remove_extra;
            var y_position = '137px';
        }
        let current_character = card_replacements.pop();
        $('#'+card_number).append('<span data-letter="'+replaced_letter+'" style="top: '+y_position+'; left:'+x_position+'px;" class="qwerty_characters monotype">'+current_character+'</span>');
        if (!is_find) {
            ordered_characters.push(current_character);
        }
    }
    return ordered_characters
}

function find_card(card_code) {
    /** Create a QWERTY card from user input for recreating a specific card */
    let spacebar_code = card_code.slice(26, 34);
    let recover_text = card_code;
    card_code = card_code.slice(0, 26).split('');
    $('#cards, #start, #how_many, #get_card, #find_card').hide();
    $('#the_print').show();
    card_code = add_letters_to_card(0, card_code, true);
    spacebar_code = convert_characters_to_html(spacebar_code, false);
    recover_text = convert_characters_to_html(recover_text, false);
    $('#0').append('<p><span class="spacebar_code">'+spacebar_code+'</span><p>');
    create_print_page(card_code, $('#0'), spacebar_code, recover_text, false);

}
function generate_random_character() {
    /** Generate a random character */
    let character = String.fromCharCode(Math.floor(Math.random() * 93 + 33)); //Magic numbers that give us a random digit/character
    return convert_characters_to_html(character);
}

function convert_characters_to_html(string, one_char = true) {
    /** Converts special characters into HTML entities that can be inserted onto the page.
     * Credit to Chris Baker and S.B. on https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
     * for this function.
     */
    if (one_char) {
        return string.replace(/[\u00A0-\u9999<>,\&]/m, function(i) {
            return '&#'+i.charCodeAt(0)+';';
        });
    } else {
        return string.replace(/[\u00A0-\u9999<>,\&]/igm, function(i) {
            return '&#'+i.charCodeAt(0)+';';
        });
    }
}

function create_print_page(card_code_array, card, spacebar_code, recover_text, not_find = true) {
    /** Generate page to print */
    //Get encrypted example of Amazon
    let example_web_characters = [10, 25, 10, 19, 8, 24];
    let example_web_text = '';
    example_web_characters.forEach(function(char_code) {
        example_web_text += card_code_array[char_code];
    });
    
    $('#cards').hide();
    $('#the_print').show();
    if (not_find) {
        $('#the_print').append(card.clone().prop('id', 'backup'));
    }
    $('#the_print').append('<br><p>Step 1: Type in the code letters shown on the \'spacebar\' of the card</p>' + 
        '<p>Password <span class="monotype">'+spacebar_code+'</span></p><p>Step 2: Enter your own password or secret word in encrypted form using the above keyboard</p>' + 
        '<p>Password <span class="monotype">'+spacebar_code+'******</span></p><p>Step 3: Type in the code characters for each letter of the website/service you are using</p>' + 
        '<p>Example: www.AMAZON.com use the code characters for each letter of AMAZON</p><p>Password <span class="monotype">'+spacebar_code+'******'+example_web_text+'</span></p>' + 
        '<p>Done! Your passwords will now be super strong and unique for every website</p>' + 
        '<p>If you lose your Qwerty Card and want to print out a new one, go back to this website and type in <br><span class="monotype">'+recover_text+'</span> into the Card ID field.');
    $('#the_print').append(card.clone().prop('id', 'cut_out'));
    $('#cut_out').wrapAll('<div id="dotted_background"></div>');
    $('#the_print').append('<p>Cut out Qwerty Card here</p>');
}